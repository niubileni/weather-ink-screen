## 2021-06-15 重磅更新！文件管理器！！！!
[输入图片说明](https://images.gitee.com/uploads/images/2021/0615/191443_0ed6f68a_1965908.jpeg "2.jpg")
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/xuanran.JPG) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-0.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-1.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-2.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-3.jpg) 
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/V12-4.jpg) 
### 4大功能已经完成 [点击下载](https://gitee.com/Lichengjiez/weather-ink-screen/releases)
### 时钟功能已发布，局刷5次全刷1次
### 注意事项（请勿用于商业用途）
* 感谢[GxEPD2](https://github.com/ZinggJM/GxEPD2)库提供的屏幕驱动，[改库适配大连佳显屏幕](https://goodlcd.taobao.com/shop/view_shop.htm?spm=a230r.1.14.39.7a293567D3cz3Y&user_number_id=151859855)
* 观看视频 [https://www.bilibili.com/video/BV1kQ4y1d7Jj](https://www.bilibili.com/video/BV1kQ4y1d7Jj)
* 
* 按住按键3不放，再按复位按键，即可进入模式选择界面(按键3-GPIO5 按键2-GPIO0)
* 所有界面的按键操作逻辑为：
* a.单独短按为切换选项
* b.组合按键为确认操作或调出菜单：先按着按键3不放，再短按一下按键2，此时要释放按键3才会生效（已改所有界面统一）
* 
* 按键2不可按得太快，不能在屏幕刷新的时候按，会导致屏幕死机，原因是按键2与屏幕刷新共用一个io口
* 原版U8g2_for_Adafruit_GFX库无法使用大字库，故更改了库，自行到码云或群里下载
* 其他库均可在库管理器下载的到
* 无法连接wifi请检查是否被路由器拉黑
* 无法获取天气信息请检查城市名是否填对，免费用户只能查看到地级市
* 低压休眠的请检查电池测量电路是否正常，电池电压是否大于3.2V（搭板的玩家自己给A0加上分压电路接上5V，分压后不能超过1V，否则烧ADC）
* 如原版的MOS管（排线附近）使用起来有问题可用cj3400代替，LDO使用ME6209A33PG代替
* 如无法连接8266的热点，请检查手机是否开启了智能选网模式
* 电池可以用：902030-500mah，603759-1400mah

### V012 新加功能
* （已完成）模式选择界面<br>
* （已完成）txt阅读模式，可网页上传文件<br>
* （已完成）SHT3X温湿度传感器代码支持<br>
* （已完成）时间显示模式，1分钟刷新一次<br>
### 介绍
* 仅支持2.9寸墨水屏，代号029A01和029M06。<br>
* 如需其他同尺寸的屏幕，请留言<br>
* 使用Arduino开发，使用到的库GxEPD2、U8g2_for_Adafruit_GFX、NTPClient、ArduinoJson、ESP_EEPROM。<br>
* 使用心知天气个人免费版KEY（20次/分钟），需要自己去申请，然后在配网界面输入即可。<br>
* 提供3D打印外壳，裸面版、全包版本已发布<br>
* 硬件使用[DUCK的天气墨水屏项目](http://oshwhub.com/duck/esp8266-weather-station-epaper)。<br>
* 改良版硬件，地址 [https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer](https://oshwhub.com/HalfSweet/29-EPaper-Thermo-hygrometer) 

### 版本更新介绍
#### V012
* 添加模式选择界面，修改按键决策
* txt阅读器模式
    * 最大支持2.8Mb的txt文件，可用空间的实际大小是3Mb，但需要留一点空间来生成索引和存放网页文件（大概1Mb的文本会生成20kb的索引）
    * 最多支持3个txt文件，但累计不能超过2.8Mb，OTA版本则为1.8Mb
* 修改和优化wifi连接状态提示，精简部分代码，去掉动态显示，避免频繁刷新屏幕
* 添加SHT30温湿度传感器的代码支持,检测到有就会在右下角显示，没有则不显示
* 配网界面添加8266的硬件MAC地址信息
#### v011
* 添加紫外线强度指数、相对湿度、风力等级
* 加大实况温度字体，自动居中
* 配网页面增加电压显示类型切换按钮，电压原始值 或 百分比
* 配网页面增加更多的提示

#### v010
* 美化配网web界面，使用模态框代替页面跳转，看着舒服很多
* 配网web界面，加入关于按钮，显示系统版本和状态
* 配网web界面，加入更多设置按钮，包含以下功能:
* 加入夜间不更新功能开关功能，可在配网界面开启关闭
* 加入自定义一句话功能，可在配网界面设置
#### v009
* 不记得了

### 功能简介
* 实况天气和未来2日天气
* web配网功能
* webOTA功能
* 一言功能，即屏幕中间一句话，每次更新天气更新
* 夜间不更新功能，可在配网界面开启关闭
* 自定义一句话功能，可在配网界面设置
* GB2312一二级字库+全国城市字库，完全可应付日常使用
* 配网模式时，配网信息、web信息、电压信息显示
* 开机壁纸、载入数据显示
* 低电量提示并永久休眠，小于3.3V，日历图标处显示电池电压
* 休眠电流0.026ma，工作电流120-70ma

### 已知BUG
* 开机载入数据有小几率会重启系统，EXCCAUSE Code(3),加载或存储期间的处理器内部物理地址或数据错误？
* 配网界面更换城市时无法即时更新天气数据，https get数据失败，暂无能力解决。
* 在配网页面连接无效的的WIFI会卡一段时间，有相应提示。可能是硬件问题，无法同时进行STA和AP的收发？等待提示连接失败即可操作其他。
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/%E7%83%A7%E5%BD%95%E8%AF%B4%E6%98%8E.png)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/peizhi.png)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/pw%20(1).jpg)
![](https://gitee.com/Lichengjiez/weather-ink-screen/raw/master/%E5%9B%BE%E7%89%87/pw%20(2).jpg)
